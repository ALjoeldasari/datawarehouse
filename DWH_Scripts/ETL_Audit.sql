/****** Object:  Table [DWH].[ETL_Audit]    Script Date: 20/03/2019 10:35:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [DWH].[ETL_Audit](
	[PackageName] [varchar](255) NOT NULL,
	[SourceTableName] [varchar](255) NULL,
	[TargetTableName] [varchar](255) NULL,
	[StartTime] [datetime] NULL,
	[Endtime] [datetime] NULL,
	[RunTime] [int] NULL,
	[RowsInserted] [int] NULL,
	[RowsUpdated] [int] NULL,
	[RunStatus] [char](10) NULL
) ON [PRIMARY]
GO


