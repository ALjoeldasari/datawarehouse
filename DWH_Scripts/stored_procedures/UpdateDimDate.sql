/****** Object:  StoredProcedure [DWH].[UpdateDimDate]    Script Date: 11/03/2019 15:28:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:      <Author, , Name>
-- Create Date: <Create Date, , >
-- Description: <Description, , >
-- =============================================
CREATE PROCEDURE [DWH].[UpdateDimDate]

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON

--Update Holiday information
UPDATE [DWH].[DIM_Date]
SET [IsHoliday] = 1,
   [HolidayName] = 'Christmas'
WHERE [Month] = 12
   AND [DAY] = 25

UPDATE [DWH].[DIM_Date]
SET SpecialDays = 'Valentines Day'
WHERE [Month] = 2
   AND [DAY] = 14

--Update current date flags
UPDATE [DWH].[DIM_Date]
SET CurrentYear = case when [year] = datepart(yyyy,getdate()) then 1 else 0 end,
   CurrentQuarter = case when ([year] = datepart(yyyy,getdate()) and [Quarter] = 1) then 1 else 0 end,
   CurrentMonth = case when ([year] = datepart(yyyy,getdate()) and [Month] = datepart(mm,getdate())) then 1 else 0 end,
   CurrentWeek = case when ([year] = datepart(yyyy,getdate()) and weekofYear =  datepart(ww,getdate())) then 1 else 0 end,
   CurrentDay = case when [date] = CONVERT(char(10), GetDate(),126)  then 1 else 0 end


UPDATE DWH.DIM_Date
SET DaysAgo = DATEDIFF(d,date,GETDATE()),
	WeeksAgo = DATEDIFF(ww,date,GETDATE()) ,
	MonthsAgo  = DATEDIFF(mm,date,GETDATE()) , 
	QuartersAgo = DATEDIFF(qq,date,GETDATE()) ,
	YearsAgo = DATEDIFF(yyyy,date,GETDATE()) ,
	YearToDate = CASE
				 WHEN  [date] <= GETDATE() and [year] = DATEPART(yyyy,GETDATE())  THEN 1 
			     ELSE 0 END  ,

	QuarterToDate = CASE WHEN [date] <= GETDATE() and [quarter] = DATEPART(qq,GETDATE()) and [year] = DATEPART(yyyy,GETDATE())   THEN 1 
			     ELSE 0 END  ,


	MonthToDate = CASE WHEN [date] <= GETDATE() and [month] = DATEPART(mm,GETDATE()) and [year] = DATEPART(yyyy,GETDATE())   THEN 1 
			     ELSE 0 END 
END

GO

