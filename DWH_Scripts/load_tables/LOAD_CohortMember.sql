CREATE  TABLE [DWH].[LOAD_CohortMember](
Id	nVarchar(32) NOT	NULL,
[Cohort__c] nvarchar(18) NULL,
Status__c	nVarchar(255)	NULL,
Contact__c nVarchar(255) NULL,
Cancellation_reason__c	nVarchar(255)	NULL,
True_Cancellation__c	nVarchar(255) NULL,
CreatedDate	Date		NOT NULL,
Completed_Date__c Date NULL , 
Enrolment_Date__c Date NULL,
Completed__c Bit NULL,
IsDeleted	bit	NOT NULL
)