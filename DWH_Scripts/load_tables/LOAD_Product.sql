CREATE  TABLE [DWH].[LOAD_Product](
	Id [varchar](32) NOT NULL,
	Name [varchar](255) NOT NULL,
	ProductCode [varchar](255) NOT NULL,
	Family [varchar](64) NOT NULL,
	[Description] [nvarchar](max)  NULL,
	Business_Group__c [varchar](1300) NOT NULL,
	Awarding_Body__c [varchar](255) NULL
) 