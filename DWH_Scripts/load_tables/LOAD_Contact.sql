CREATE TABLE [DWH].[LOAD_Contact](
Id	varchar(32)	NOT NULL,
[AccountId] varchar(32) null,
MailingCountry	nVarchar(128)	NULL,
MailingCity	nVarchar(64)	NULL,
Gender__c	Varchar(10)	NULL,
Name	nVarchar(128)	NULL,
AAT_Membership_Number__c	nVarchar(255)	NULL,
ACCA_Membership_Number__c	nVarchar	(255)	NULL,
CIPD_Membership__c	nVarchar(255)	NULL,
CMI_Membership_Number__c	nVarchar(255)	NULL,
Email	nVarchar(255)	NULL,
IsDeleted	BIT	NOT NULL,
LastName	nVarchar(128)	 NULL,
FirstName	nVarchar(128)	 NULL,
Region__c	Char(5)	 NULL,
Department	nVarchar(128)	NULL
)