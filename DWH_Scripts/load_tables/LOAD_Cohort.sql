CREATE TABLE [DWH].[LOAD_Cohort](
Id	nVarchar(32)	NOT NULL,
Tutor__c	nVarchar(32)	NULL,
College__c	nVarchar(32) NULL,
Name	nVarchar(128)	NULL,
Status__c	nVarchar(255)	NOT NULL,
[Faculty__c] nVarchar(32) NULL,
CreatedDate Datetime NULL,
Start_Date__c Date NULL, 
End_Date__c Date NULL,
Product__c nVarchar(32) NULL
)