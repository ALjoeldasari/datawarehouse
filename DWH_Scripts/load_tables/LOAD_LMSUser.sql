CREATE  TABLE [DWH].[LOAD_LMSUser](
Id	nVarchar(32)	NOT NULL,
External_Id__c	nVarchar(64)	NOT NULL,
Username__c	nVarchar(255) NULL,
LMS_Instance__c nVarchar(32) NULL,
Sync_Status__c	nVarchar(255)	NOT NULL,
Contact__c nVarchar(32) NULL,
CreatedDate	Date		NOT NULL,
Last_Access_Date__c	Date		NULL,
Is_Suspended__c	BIT	NOT NULL,
SourceSystem	nVarchar(255)	NOT NULL,
IsDeleted	BIT	NOT NULL
)