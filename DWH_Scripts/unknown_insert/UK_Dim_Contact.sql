USE [datawarehouse]
GO

SET IDENTITY_INSERT DWH.[DIM_Contact] ON 

INSERT INTO [DWH].[DIM_Contact]
           (ContactKey
		   ,[ContactId]
           ,[AccountName]
           ,[MailingCountry]
           ,[MailingCity]
           ,[Gender]
           ,[ContactName]
           ,[AATMembershipID]
           ,[ACCAMembershipID]
           ,[CIPDMembershipID]
           ,[CMIMembershipID]
           ,[ContactEmail]
           ,[IsDeleted]
           ,[LastName]
           ,[FirstName]
           ,[Region]
           ,[Department])
     VALUES
           (-1
		   ,'UNKNOWN'
           ,'UNKNOWN'
           ,'UNKNOWN'
           ,'UNKNOWN'
           ,'UNKNOWN'
           ,'UNKNOWN'
           ,'UNKNOWN'
           ,'UNKNOWN'
           ,'UNKNOWN'
           ,'UNKNOWN'
           ,'UNKNOWN'
           ,0
           ,'UNKNOWN'
           ,'UNKNOWN'
           ,'N/A'
           ,'UNKNOWN'
		   )

SET IDENTITY_INSERT DWH.[DIM_Contact] OFF 

GO


