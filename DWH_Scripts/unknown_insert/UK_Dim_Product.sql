USE [datawarehouse]
GO

SET IDENTITY_INSERT DWH.[DIM_Product] ON 

INSERT INTO [DWH].[DIM_Product]
           (ProductKey
		   ,[ProductID]
           ,[ProductName]
           ,[ProductCode]
           ,[Family]
           ,[Description]
           ,[BusinessGroup]
           ,[AwardingBody])
     VALUES
           (-1
		   ,'UNKNOWN'
           ,'UNKNOWN'
           ,'UNKNOWN'
           ,'UNKNOWN'
           ,'UNKNOWN'
           ,'UNKNOWN'
           ,'UNKNOWN'
		   )

SET IDENTITY_INSERT DWH.[DIM_Product] OFF 

GO


