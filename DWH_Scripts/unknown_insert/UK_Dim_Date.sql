USE [datawarehouse]
GO

USE [datawarehouse]
GO

INSERT INTO [DWH].[DIM_Date]
           ([DateKey]
           ,[Date]
           ,[Day]
           ,[DaySuffix]
           ,[Weekday]
           ,[WeekDayName]
           ,[WeekDayNameShort]
           ,[WeekDayNameFirstLetter]
           ,[DOWInMonth]
           ,[DayOfYear]
           ,[WeekOfMonth]
           ,[WeekOfYear]
           ,[Month]
           ,[MonthName]
           ,[MonthNameShort]
           ,[MonthNameFirstLetter]
           ,[Quarter]
           ,[QuarterName]
           ,[Year]
           ,[MMYYYY]
           ,[MonthYear]
           ,[IsWeekend]
           ,[IsHoliday]
           ,[HolidayName]
           ,[SpecialDays]
           ,[FirstDateofYear]
           ,[LastDateofYear]
           ,[FirstDateofQuarter]
           ,[LastDateofQuarter]
           ,[FirstDateofMonth]
           ,[LastDateofMonth]
           ,[FirstDateofWeek]
           ,[LastDateofWeek]
           ,[CurrentYear]
           ,[CurrentQuarter]
           ,[CurrentMonth]
           ,[CurrentWeek]
           ,[CurrentDay]
           ,[DaysAgo]
           ,[WeeksAgo]
           ,[MonthsAgo]
           ,[QuartersAgo]
           ,[YearsAgo]
           ,[YearToDate]
           ,[QuarterToDate]
           ,[MonthToDate]
           ,[FiscalDayOfYear]
           ,[FiscalWeekOfYear]
           ,[FiscalMonth]
           ,[FiscalQuarter]
           ,[FiscalQuarterName]
           ,[FiscalYear]
           ,[FiscalMonthYear]
           ,[FiscalMMYYY]
           ,[FiscalFirstDayOfMonth]
           ,[FiscalLastDayOfMonth]
           ,[FiscalFirstDayOfQuarter]
           ,[FiscalLastDayOfQuarter]
           ,[FiscalFirstDayOfYear]
           ,[FiscalLastDayOfYear])
     VALUES
           (-1
           ,'1900-01-01'
           ,0
           ,'N/A'
           ,0
           ,'UNKNOWN'
           ,'N/A'
           ,'N/A'
           ,0
           ,0
           ,0
           ,0
           ,0
           ,'UNKNOWN'
           ,'N/A'
           ,'N/A'
           ,0
           ,'N/A'
           ,0
           ,'N/A'
           ,'UNKNOWN'
           ,0
           ,0
           ,'UNKNOWN'
           ,'UNKNOWN'
           ,'1900-01-01'
           ,'1900-01-01'
           ,'1900-01-01'
           ,'1900-01-01'
           ,'1900-01-01'
           ,'1900-01-01'
           ,'1900-01-01'
           ,'1900-01-01'
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,'UNKNOWN'
           ,'N/A'
           ,'UNKNOWN'
           ,'N/A'
           ,'1900-01-01'
           ,'1900-01-01'
           ,'1900-01-01'
           ,'1900-01-01'
           ,'1900-01-01'
           ,'1900-01-01'
)

GO