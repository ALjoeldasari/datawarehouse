USE [datawarehouse]
GO

SET IDENTITY_INSERT DWH.[DIM_LMSUser] ON 

INSERT INTO [DWH].[DIM_LMSUser]
           (LMSUserKey
		   ,[Username]
           ,[LMSUserId]
           ,[ExternalId]
           ,[LastAccessDate]
           ,[SyncStatus]
           ,[IsDeleted]
           ,[CreatedDate]
           ,[IsSuspended]
           ,[SourceSystem]
           ,[LMSInstance]
           ,[LearnerStatus])
     VALUES
           (-1
		   ,'UNKNOWN'
           ,'UNKNOWN'
           ,'UNKNOWN'
           ,'1900-01-01'
           ,'UNKNOWN'
           ,0
           ,'1900-01-01'
           ,0
           ,'UNKNOWN'
           ,'UNKNOWN'
           ,'UNKNOWN'
		   )
GO



SET IDENTITY_INSERT DWH.[DIM_LMSUser] OFF 

GO


