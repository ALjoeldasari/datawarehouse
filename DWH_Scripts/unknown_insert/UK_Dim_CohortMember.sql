USE [datawarehouse]
GO

SET IDENTITY_INSERT DWH.DIM_CohortMember ON 

INSERT INTO [DWH].[DIM_CohortMember]
           ([CohortMemberKey], 
		    [CohortId]
           ,[CohortMemberId]
           ,[Tutor]
           ,[CohortMemberStatus]
           ,[College]
           ,[CohortName]
           ,[Faculty]
           ,[IsDeleted]
           ,[CancellationReason]
           ,[TrueCancellationReason]
           ,[Defer]
           ,[CohortStatus]
           ,[CreatedDate]
           ,[SCDStartDate]
           ,[SCDEndDate]
           ,[SCDVersion]
           ,[SCDCurrent]
           ,[SCDHash])
     VALUES
           (-1 
		    ,'UNKNOWN'
           ,'UNKNOWN'
           ,'UNKNOWN'
           ,'UNKNOWN'
           ,'UNKNOWN'
           ,'UNKNOWN'
           ,'UNKNOWN'
           ,0
           ,'UNKNOWN'
           ,'UNKNOWN'
           ,0
           ,'UNKNOWN'
           ,'1900-01-01'
           ,'1900-01-01'
           ,'1900-01-01'
           ,0
           ,0
           ,CAST('UNKNOWN' AS varbinary(MAX)))

		   SET IDENTITY_INSERT DWH.DIM_CohortMember OFF
		   
GO