-- PRODUCT stage

SELECT [Id] AS ProductID,
		[Name] AS ProductName,
		[ProductCode] AS ProductCode,
		[Family] AS Family,
		[Description] AS Description,
		[Business_Group__c] AS BusinessGroup,
		[Awarding_Body__c] AS AwardingBody
   FROM DWH.LOAD_Product

--------------------------------------------------------

-- LMSUSER stage

	 SELECT LU.[Username__c] AS Username,
			LU.[Id] AS LMSUserId,
			LU.[External_Id__c] AS ExternalId,			
			LU.[Sync_Status__c] AS SyncStatus,			
			LU.[CreatedDate] AS CreatedDate,
			LU.[Last_Access_Date__c] AS LastAccessDate,
			LU.[Is_Suspended__c] AS IsSuspended,			
  CASE WHEN LU.[Last_Access_Date__c]<=14 THEN 'Active'
	   WHEN LU.[Last_Access_Date__c] BETWEEN 15 AND 35 THEN 'At Risk'
	   WHEN LU.[Last_Access_Date__c]>35 THEN 'Inactive'
	   ELSE 'Not Logged In '
	 END AS LearnerStatus,
	        LI.[Name] AS LMSInstance,
		    'SalesForce' AS SourceSystem,
			LU.[IsDeleted] AS IsDeleted	 
	   FROM DWH.LOAD_LmsUser AS LU
	   JOIN DWH.LOAD_LmsInstance AS LI
	     ON LU.LMS_Instance__c=LI.Id
		 
		 
--------------------------------------------------------

-- CONTACT stage

SELECT C.[Id] AS ContactId,
	    A.[Name] AS AccountName,
	    C.[Name] AS ContactName,
		C.[LastName] AS LastName,
		C.[FirstName] AS FirstName,
	    C.[Gender__c] AS Gender,
	    C.[Email] AS ContactEmail,
	    C.[MailingCity] AS MailingCity,
	    C.[MailingCountry] AS MailingCountry,
	    C.[Region__c] AS Region,
	    C.[Department] AS Department,
	    C.[AAT_Membership_Number__c] AS AATMembershipID,
	    C.[ACCA_Membership_Number__c] AS  ACCAMembershipID,
	    C.[CIPD_Membership__c] AS CIPDMembershipID,
	    C.[CMI_Membership_Number__c] AS CMIMembershipID,
	    C.[IsDeleted] AS IsDeleted 
	FROM DWH.LOAD_ACCOUNT A
	JOIN DWH.LOAD_CONTACT C
	ON C.AccountId = A.Id

--------------------------------------------------------

-- COHORT MEMBER stage

	SELECT C.[Id] AS CohortId,
		    CM.[Id] AS CohortMemberId,
			C.[Name] AS CohortName,
			CM.[Status__c] AS CohortMemberStatus,
			C.[Status__c] AS CohortStatus,
			C.[Tutor__c] AS Tutor,
			A.[Name] AS Faculty,
			C.[College__c] AS College,
			CM.[Cancellation_reason__c] AS CancellationReason,
			CM.[True_Cancellation__c] AS TrueCancellationReason,
  CASE WHEN CM.[True_Cancellation__c] = 'Deferral' THEN 1
	   ELSE 0 
	 END AS Defer,
		    CM.[CreatedDate] AS CreatedDate,
		    CM.[IsDeleted] AS IsDeleted
	   FROM DWH.LOAD_Cohort C
       JOIN DWH.LOAD_CohortMember CM
		 ON CM.Cohort__c = C.Id
	   JOIN DWH.LOAD_ACCOUNT A
	     ON C.Faculty__c = A.Id

--------------------------------------------------------

-- FACT STUDENT stage
		 SELECT NULL AS CohortId, 
			    NULL AS CohortMemberId, 
				C.id AS ContactId, 
				L.id AS LMSUserId, 
				NULL AS ProductId, 
				NULL AS CohortCreatedDate, 
				NULL AS CohortCompletedDate, 
				NULL AS CohortMemberEnrolmentDate, 
				NULL AS CohortStartDate, 
				NULL AS CohortEndDate 
		    FROM DWH.LOAD_LmsUser L --64671 
			JOIN DWH.LOAD_Contact C --64671 
			ON C.id = L.Contact__c 
			JOIN DWH.LOAD_Account A --64671 
			ON A.ID = C.AccountId 
UNION
		SELECT C.id AS CohortId, 
				CM.id AS CohortMemberId, 
				CC.id asContactId, 
				NULL AS LMSUserId, 
				P.id AS ProductId, 
				C.CreatedDate AS CohortCreatedDate, 
				CM.Completed_Date__c AS CohortCompletedDate, 
				CM.Enrolment_Date__c CohortMemberEnrolmentDate, 
				C.Start_Date__c CohortStartDate, 
				C.End_Date__c CohortEndDate 
		FROM   DWH.LOAD_CohortMember CM 
			JOIN DWH.LOAD_Cohort C 
			ON C.Id = CM.Cohort__c 
			JOIN DWH.LOAD_Product P 
			ON P.id = C.Product__c 
			JOIN DWH.LOAD_Contact CC 
			ON CC.id = CM.Contact__c 
			JOIN DWH.LOAD_Account A 
			ON A.id = CC.AccountId 

