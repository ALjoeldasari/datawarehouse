/****** Object:  Table [DWH].[DIM_Date]    Script Date: 19/03/2019 14:26:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [DWH].[DIM_Date](
	[DateKey] [int] NOT NULL,
	[Date] [date] NOT NULL,
	[Day] [tinyint] NOT NULL,
	[DaySuffix] [char](3) NOT NULL,
	[Weekday] [tinyint] NOT NULL,
	[WeekDayName] [varchar](10) NOT NULL,
	[WeekDayNameShort] [char](3) NOT NULL,
	[WeekDayNameFirstLetter] [char](3) NOT NULL,
	[DOWInMonth] [tinyint] NOT NULL,
	[DayOfYear] [int] NOT NULL,
	[WeekOfMonth] [tinyint] NOT NULL,
	[WeekOfYear] [tinyint] NOT NULL,
	[Month] [tinyint] NOT NULL,
	[MonthName] [varchar](10) NOT NULL,
	[MonthNameShort] [char](3) NOT NULL,
	[MonthNameFirstLetter] [char](3) NOT NULL,
	[Quarter] [tinyint] NOT NULL,
	[QuarterName] [varchar](6) NOT NULL,
	[Year] [int] NOT NULL,
	[MMYYYY] [char](6) NOT NULL,
	[MonthYear] [char](7) NOT NULL,
	[IsWeekend] [bit] NOT NULL,
	[IsHoliday] [bit] NOT NULL,
	[HolidayName] [varchar](20) NULL,
	[SpecialDays] [varchar](20) NULL,
	[FirstDateofYear] [date] NULL,
	[LastDateofYear] [date] NULL,
	[FirstDateofQuarter] [date] NULL,
	[LastDateofQuarter] [date] NULL,
	[FirstDateofMonth] [date] NULL,
	[LastDateofMonth] [date] NULL,
	[FirstDateofWeek] [date] NULL,
	[LastDateofWeek] [date] NULL,
	[CurrentYear] [int] NULL,
	[CurrentQuarter] [int] NULL,
	[CurrentMonth] [int] NULL,
	[CurrentWeek] [int] NULL,
	[CurrentDay] [int] NULL,
	[DaysAgo] [int] NULL,
	[WeeksAgo] [int] NULL,
	[MonthsAgo] [int] NULL,
	[QuartersAgo] [int] NULL,
	[YearsAgo] [int] NULL,
	[YearToDate] [int] NULL,
	[QuarterToDate] [int] NULL,
	[MonthToDate] [int] NULL,
	[FiscalDayOfYear] [int] NULL,
	[FiscalWeekOfYear] [tinyint] NULL,
	[FiscalMonth] [tinyint] NULL,
	[FiscalQuarter] [tinyint] NULL,
	[FiscalQuarterName] [varchar](10) NULL,
	[FiscalYear] [char](4) NULL,
	[FiscalMonthYear] [varchar](10) NULL,
	[FiscalMMYYY] [varchar](6) NULL,
	[FiscalFirstDayOfMonth] [date] NULL,
	[FiscalLastDayOfMonth] [date] NULL,
	[FiscalFirstDayOfQuarter] [date] NULL,
	[FiscalLastDayOfQuarter] [date] NULL,
	[FiscalFirstDayOfYear] [date] NULL,
	[FiscalLastDayOfYear] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[DateKey] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


