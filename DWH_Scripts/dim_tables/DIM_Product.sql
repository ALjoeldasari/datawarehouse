CREATE  TABLE [DWH].[DIM_Product](
ProductKey	Int		NOT NULL IDENTITY(1,1),
		ProductID	Varchar(32)	NOT NULL,
		ProductName	Varchar(255)	NOT NULL,
		ProductCode	Varchar(255)	NOT NULL,
		Family	Varchar(64)	NOT NULL,
		Description	nVarchar(max) NULL,
		BusinessGroup	Varchar(1300)	NOT NULL,
		AwardingBody	Varchar(255)	NULL,
		CONSTRAINT dim_product_pk PRIMARY KEY (ProductKey)
) 