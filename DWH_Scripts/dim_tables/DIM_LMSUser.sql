CREATE TABLE [DWH].[DIM_LMSUser](
LMSUserKey	Int		NOT NULL IDENTITY(1,1),
Username	Varchar(255) NULL,
LMSUserId	Varchar(32)	NOT NULL,
ExternalId	Varchar(64)	NOT NULL,
LastAccessDate	Date		NULL,
SyncStatus	Varchar(255)	NOT NULL,
IsDeleted	BIT	NOT NULL,
CreatedDate	Date		NOT NULL,
IsSuspended	BIT	NOT NULL,
SourceSystem	Varchar(255)	NOT NULL,
LMSInstance	Varchar(80)	NOT NULL,
LearnerStatus	Varchar(16)	NOT NULL,
CONSTRAINT dim_lmsuser_pk PRIMARY KEY (LMSUserKey)
)