CREATE TABLE [DWH].[STAGE_Student](
CohortId	Varchar(32)	NULL,
CohortMemberId	Varchar(32)	NULL,
ContactId	Varchar(32)	NULL,
LMSUserId	Varchar(32)	NULL,
ProductId	Varchar(32)	NULL,
CohortCompletedFlag	Bit	 NULL,
CohortCreatedDate	Date NULL,
CohortCompletedDate	Date NULL,
CohortMemberEnrolmentDate Date NULL,
CohortStartDate	Date  NULL,
CohortEndDate	Date  NULL
) 