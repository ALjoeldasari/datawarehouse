CREATE TABLE  [DWH].[STAGE_Contact](
ContactId	varchar(32)	NOT NULL,
AccountName	Varchar(255)	NULL,
MailingCountry	Varchar(128)	NULL,
MailingCity	Varchar(64)	NULL,
Gender	Varchar(10)	NULL,
ContactName	Varchar(128)	NULL,
AATMembershipID	Varchar(255)	NULL,
ACCAMembershipID	Varchar(255)	NULL,
CIPDMembershipID	Varchar(255)	NULL,
CMIMembershipID	Varchar(255)	NULL,
ContactEmail	Varchar(255)	NULL,
IsDeleted	bit	NOT NULL,
LastName	Varchar(128) NULL,
FirstName	Varchar(128) NULL,
Region	Char(5)	 NULL,
Department	Varchar(128) NULL
CONSTRAINT stg_contact_pk PRIMARY KEY (ContactId)
)