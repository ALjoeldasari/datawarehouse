CREATE TABLE [DWH].[STAGE_CohortMember](
CohortId	Varchar(32)	NOT NULL,
CohortMemberId	Varchar(32) NOT	NULL,
Tutor	Varchar(32)	NULL,
CohortMemberStatus	Varchar(255)	NULL,
College	Varchar(32)	NULL,
CohortName	Varchar(128)	NULL,
Faculty	Varchar(255)	NULL,
IsDeleted	Bit	NOT NULL,
CancellationReason	varchar(255)	NULL,
TrueCancellationReason	varchar(255)	NULL,
Defer	Bit	NOT NULL,
CohortStatus	Varchar(255)	NOT NULL,
CreatedDate	Date		NOT NULL,
 CONSTRAINT stg_cohortmember_pk PRIMARY KEY (CohortId, CohortMemberId)
)