CREATE TABLE [DWH].[STAGE_Product](
		ProductID	Varchar(32)	NOT NULL,
		ProductName	Varchar(255)	NOT NULL,
		ProductCode	Varchar(255)	NOT NULL,
		Family	Varchar(64)	NOT NULL,
		Description	nVarchar(max) NULL,
		BusinessGroup	Varchar(1300)	NOT NULL,
		AwardingBody	Varchar(255)	NULL
		CONSTRAINT stg_product_pk PRIMARY KEY (ProductID)
) 